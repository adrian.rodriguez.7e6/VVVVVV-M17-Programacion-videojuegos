# VVVVVV M17

Título y Descripción: 
VVVVVV consiste en un juego de de plataformeo por niveles en el cual tienes que llegar al final del juego y conseguir un cofre esquivando enemigos, proyectiles y trampas mediante el uso del cambio de gravedad.

Instalación:
Estrae los archivos de la carpeta comprimida y ejecuta el .exe

Uso:
Los controles son básicos. Con la flecha derecha te mueves a la derecha y con la flechita izquierda te mueves a la izquierda. Con el espacio cambias la gravedad y con el escape pausas el juego.

Características:
Solo hay un modo de juego y tiene 5 niveles. El objetivo es llegar al final y conseguir el cofre de la victoria.

Requisitos minimos:

OS: Windows XP
Processor: 2 GHz
Memory: 256MB
Graphics: Direct X9.0c Compatible Card
DirectX®: DirectX® 9.0c
Hard Drive: 500MB
Sound: Standard audio


Created by Adrián Rodríguez Bolín