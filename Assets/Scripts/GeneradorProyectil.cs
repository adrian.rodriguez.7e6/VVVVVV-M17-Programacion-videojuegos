using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorProyectil : MonoBehaviour
{
    [SerializeField] private Transform puntoDeDisparo;
    [SerializeField] private GameObject proyectilPrefab;


    public float velocidadProyectil = 10.0f;
    public float tiempoEntreDisparos = 2.0f;

    void Start()
    {
        // Inicia la generación de proyectiles a intervalos regulares
        InvokeRepeating("DispararProyectil", 0f, 2);
    }

    void DispararProyectil()
    {
        GameObject proyectil = Instantiate(proyectilPrefab, puntoDeDisparo.position, puntoDeDisparo.rotation);
        Rigidbody2D rb = proyectil.GetComponent<Rigidbody2D>();

        if (rb != null)
        {
            rb.velocity = puntoDeDisparo.right * velocidadProyectil;
        }
        else
        {
            Debug.LogError("El proyectil no tiene un componente Rigidbody2D.");
        }
    }
}
