using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrullaEnemigo : MonoBehaviour
{
    public Transform puntoIzquierdo;
    public Transform puntoDerecho;
    public float velocidad = 2.0f;

    private bool moviendoseADerecha = true;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent <SpriteRenderer>();
    }

    void Update()
    {
        if (moviendoseADerecha)
        {
            transform.Translate(Vector2.right * velocidad * Time.deltaTime);
            if (!spriteRenderer.flipX)
            {
                spriteRenderer.flipX = true; // Girar el sprite horizontalmente
            }
        }
        else
        {
            transform.Translate(Vector2.left * velocidad * Time.deltaTime);
            if (spriteRenderer.flipX)
            {
                spriteRenderer.flipX = false; // Restablecer la orientación del sprite
            }
        }

        if (transform.position.x >= puntoDerecho.position.x)
        {
            moviendoseADerecha = false;
        }
        else if (transform.position.x <= puntoIzquierdo.position.x)
        {
            moviendoseADerecha = true;
        }
    }
}
