using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionBullet : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Personaje")
        {
            if (this.tag == "Trap")
            {
                Debug.Log("Detecta bola");
                Movement_Character.actualPlayer.Die();
            }
        }
    }
}
