using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public int sceneId;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        sceneId = SceneManager.GetActiveScene().buildIndex;
        if (collision.name == "Personaje")
        {
            if (this.tag == "NextLevel")
            {
                SceneManager.LoadScene(sceneId+1, LoadSceneMode.Single);
            }
            else if (this.tag == "PreviousLevel")
            {
                SceneManager.LoadScene(sceneId - 1, LoadSceneMode.Single);
            }
        }
    }
}
