using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public AudioSource audioSource;

    void Start()
    {
        // Aseg�rate de asignar el AudioSource en el Inspector
        if (audioSource == null)
        {
            Debug.LogError("No se ha asignado un AudioSource en el Inspector.");
        }
    }

    public void ReproducirMusica()
    {
        if (audioSource != null)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }

    public void PararMusica()
    {
        if (audioSource != null)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
    }

    public void ReiniciarMusica()
    {
        PararMusica(); // Para la canci�n actual
        ReproducirMusica(); // Vuelve a reproducir la canci�n desde el principio
    }
}
