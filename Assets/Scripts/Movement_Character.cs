using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;


public class Movement_Character : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform GroundCheck;
    [SerializeField] private Transform RoofCheck;
    [SerializeField] private LayerMask GroundLayer;

    [SerializeField] private AudioSource dieSoundEffect;
    [SerializeField] private AudioSource jumpSoundEffect;

    public static Movement_Character actualPlayer;

    public float velocity;
    private bool lookRight = true;
    private Animator animator;

    public void Awake()
    {
        if (actualPlayer == null)
        {
            actualPlayer = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    public void Update()
    {
        ProcessMovement();
        GravityChange();
    }

    public void ProcessMovement()
    {
        //Logica de movimiento
        float movement = Input.GetAxis("Horizontal");

        if (movement != 0f)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        rigidBody.velocity = new Vector2(movement*velocity, rigidBody.velocity.y);

        OrientationManagement(movement);
    }

    public void OrientationManagement(float movement)
    {
        //Rotate the sprite with respect to where the character is looking
        if ((lookRight == true && movement < 0) || (lookRight == false && movement > 0))
        {
            lookRight = !lookRight;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }

    public void GravityChange()
    {
        //Debug.Log(IsGrounded());
        //Debug.Log(IsRoofed());
        if (Input.GetKeyDown(KeyCode.Space) && (IsGrounded() || IsRoofed()))
        {
            jumpSoundEffect.Play();
            rigidBody.gravityScale *= -1;
            spriteRenderer.flipY = !spriteRenderer.flipY;
        }
    }

    private bool IsGrounded()
    {
        //Debug.Log("entra a grounded");
        return Physics2D.OverlapCircle(GroundCheck.position,0.2f, GroundLayer);
    }

    private bool IsRoofed()
    {
        //Debug.Log("entra a roofed");
        return Physics2D.OverlapCircle(RoofCheck.position, 0.2f, GroundLayer);
    }

    public void ResetStats()
    {
        transform.position = new Vector3(-6.36f, -2.81f, 0);
        rigidBody.gravityScale = 3f;
        spriteRenderer.flipY= false;
    }

    public void Die()
    {
        Debug.Log("funcion die");
        animator.SetTrigger("death");
    }

    public void DieSoundEffect()
    {
        dieSoundEffect.Play();
    }
}
